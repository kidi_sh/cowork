class SitemapController < ApplicationController
  def index
    @buildings = Building.all
    @blogs = Blog.all
    @events = Event.all
    @tags = Tag.all

    respond_to do |format|
      format.xml
    end
  end
end
