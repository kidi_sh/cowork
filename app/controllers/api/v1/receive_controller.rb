class Api::V1::ReceiveController < Api::V1::BaseController
  respond_to :json

  before_action :authenticate

  def xendit
    if request.get?

      respond_to do |format|
        format.any { render :json => {:response => 'xendit only' },:status => 200  }
      end

    elsif request.post?

      if params[:status] == "COMPLETED"
        @sub = Subscription.find(params[:external_id])
        if @sub
          @sub.update_attributes(:paid => true )
          respond_to do |format|
            format.any { render :json => {:response => 'Success' },:status => 200  }
          end
        else
          respond_to do |format|
            format.any { render :json => {:response => 'Fail' },:status => 401  }
          end
        end
      else
        respond_to do |format|
          format.any { render :json => {:response => 'Fail' },:status => 401  }
        end
      end

    end
  end

  private

  def authenticate

    api_key = request.headers["X-CALLBACK-TOKEN"]
    if Rails.env.production?
      token = "NDY4NTkxMzJmNThiNzM0OTZhZTQwZDExZjZmMTQ5NDQyOTUyODE1Mg=="
    else
      token = "ODU3NTkxMzJmNThiNzM0OTZhZTQwZDExZjZmMTQ5NDQyOTUyODE1MQ=="
    end
    unless api_key == token
      head status: :unauthorized
      return false
    end

  end

end
