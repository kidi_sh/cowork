class Managers::CareersController < CareersController
  layout 'manager'
  before_filter :authenticate_manager!

  def index
    @careers = Career.all
  end
  
end
