class Managers::CategoriesController < CategoriesController
  layout 'manager'
  before_filter :authenticate_manager!

  def index
    @categories = Category.all
  end
end
