class BuildingsController < ApplicationController
  before_action :set_building, only: [:show]

  def index
    @buildings = Building.all.order(available: :desc)
    @oncoming = Building.where("available = ?", false)
  end

  def show
    @plans = Plan.where("building_id = ?", @building.id).order(price: :asc)
    @utm_source = request.params["utm_source"] ? request.params["utm_source"] : "Website"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_building
      @building = Building.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def building_params
      params.require(:building).permit(:name)
    end
end
