class EnquiriesController < ApplicationController
  before_action :set_enquiry, only: [:show, :edit, :update, :destroy]

  # GET /enquiries
  # GET /enquiries.json
  # def index
  #   @enquiries = Enquiry.all
  # end

  # GET /enquiries/1
  # GET /enquiries/1.json
  def show
  end

  # GET /enquiries/new
  def new
    @enquiry = Enquiry.new
    @utm_source = request.params["utm_source"] ? request.params["utm_source"] : "Website"
  end

  # GET /enquiries/1/edit
  # def edit
  # end

  # POST /enquiries
  # POST /enquiries.json
  def create
    params = request.params
    source = params["utm_source"].present? ? params["utm_source"] : "website"
    temp_param = enquiry_params.merge({source: source})
    @enquiry = Enquiry.new(temp_param)
    
    respond_to do |format|
      if @enquiry.save
        #binding.pry()
        EnquiryMailer.leads(@enquiry).deliver_now
        EnquiryMailer.thanks(@enquiry).deliver_now
        format.html { redirect_to enquiry_path(@enquiry), notice: 'Enquiry was successfully created.' }
        format.json { render :show, status: :created, location: @enquiry }
      else
        format.html { redirect_to(:back) }
        format.json { render json: @enquiry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /enquiries/1
  # PATCH/PUT /enquiries/1.json
  # def update
  #   respond_to do |format|
  #     if @enquiry.update(enquiry_params)
  #       format.html { redirect_to @enquiry, notice: 'Enquiry was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @enquiry }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @enquiry.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /enquiries/1
  # DELETE /enquiries/1.json
  def destroy
    @enquiry.destroy
    respond_to do |format|
      format.html { redirect_to enquiries_url, notice: 'Enquiry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enquiry
      @enquiry = Enquiry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enquiry_params
      params.require(:enquiry).permit(:name,:last_name, :phone, :email, :building_id, :plan, :enquiry)
    end
end
