class Blog < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  has_many :taggings
  has_many :tags, through: :taggings

  belongs_to :manager
  belongs_to :author
  has_attached_file :banner, styles: { medium: "300x300>", thumb: "100x100>" }, :path => ":rails_root/public/images/files/:class/:attachment/:id/:style/:filename",:url => "/images/files/:class/:attachment/:id/:style/:filename"
  validates_attachment_content_type :banner, content_type: /\Aimage\/.*\z/

  def all_tags=(names)
    self.tags = names.split(",").map do |name|
        Tag.where(name: name.strip).first_or_create!
    end
  end

  def all_tags
    self.tags.map(&:name).join(", ")
  end

  def self.tagged_with(name)
    Tag.find_by_name!(name).blogs
  end

end
