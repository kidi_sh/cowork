class Author < ActiveRecord::Base
  has_many :blogs

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/manager/:class/:attachment/:id/:style/:filename",:url => "/images/manager/:class/:attachment/:id/:style/:filename"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
end
