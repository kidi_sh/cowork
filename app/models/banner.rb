class Banner < ActiveRecord::Base
  belongs_to :building

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/manager/:class/:attachment/:id/:style/:filename",:url => "/images/manager/:class/:attachment/:id/:style/:filename"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
