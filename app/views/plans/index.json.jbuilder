# json.array! @plans, partial: 'plans/plan', as: :plan
json.array!(@plans) do |plan|
  json.extract! plan, :id
  json.name plan.name
  json.price number_to_currency(plan.price, :unit => "IDR ")
  json.building_id plan.building.id
  json.building_name plan.building.name
  json.location_id plan.building.location.id
end
