json.extract! career, :id, :title, :description, :requirement, :created_at, :updated_at
json.url career_url(career, format: :json)
