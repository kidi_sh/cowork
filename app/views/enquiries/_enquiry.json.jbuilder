json.extract! enquiry, :id, :name, :phone, :email, :building_id, :enquiry, :created_at, :updated_at
json.url enquiry_url(enquiry, format: :json)
