json.extract! evhive_connect, :id, :name, :email, :startup, :need, :created_at, :updated_at
json.url evhive_connect_url(evhive_connect, format: :json)
