xml.instruct!

xml.urlset('xmlns' => "http://www.sitemaps.org/schemas/sitemap/0.9", 'xmlns:xsi' => "http://www.w3.org/2001/XMLSchema-instance", 'xsi:schemaLocation' => "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd") do
  xml.url do
    xml.loc root_url
    xml.changefreq("yearly")
    xml.priority "1.0"
  end
  @buildings.each do |building|
    xml.url do
      xml.loc building_url(building)
      xml.changefreq("never")
      xml.priority "0.8"
      xml.lastmod building.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%2N%:z")
    end
  end

  @blogs.each do |blog|
    xml.url do
      xml.loc blog_url(blog)
      xml.changefreq("never")
      xml.priority "0.64"
      xml.lastmod blog.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%2N%:z")
    end
  end

  @events.each do |event|
    xml.url do
      xml.loc event_url(event)
      xml.changefreq("never")
      xml.priority "0.51"
      xml.lastmod event.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%2N%:z")
    end
  end

  @tags.each do |tag|
    xml.url do
      xml.loc tag_url(tag)
      xml.changefreq("never")
      xml.priority "0.51"
      xml.lastmod tag.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%2N%:z")
    end
  end
end