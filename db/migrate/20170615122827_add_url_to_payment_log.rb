class AddUrlToPaymentLog < ActiveRecord::Migration
  def change
    add_column :payment_logs, :url, :string
  end
end
