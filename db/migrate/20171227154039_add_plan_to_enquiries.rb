class AddPlanToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :plan, :string
  end
end
