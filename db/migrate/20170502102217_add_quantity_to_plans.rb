class AddQuantityToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :quantity, :integer
  end
end
