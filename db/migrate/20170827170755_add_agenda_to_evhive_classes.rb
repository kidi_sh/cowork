class AddAgendaToEvhiveClasses < ActiveRecord::Migration
  def change
    add_column :evhive_classes, :agenda, :text
  end
end
