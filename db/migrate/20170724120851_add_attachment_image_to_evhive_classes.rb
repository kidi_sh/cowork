class AddAttachmentImageToEvhiveClasses < ActiveRecord::Migration
  def self.up
    change_table :evhive_classes do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :evhive_classes, :image
  end
end
