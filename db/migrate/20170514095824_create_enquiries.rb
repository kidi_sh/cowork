class CreateEnquiries < ActiveRecord::Migration
  def change
    create_table :enquiries do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.integer :building_id
      t.text :enquiry

      t.timestamps null: false
    end
  end
end
