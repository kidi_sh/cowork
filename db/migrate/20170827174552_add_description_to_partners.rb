class AddDescriptionToPartners < ActiveRecord::Migration
  def change
    add_column :partners, :description, :text
    add_column :partners, :facebook, :string
    add_column :partners, :twitter, :string
    add_column :partners, :instagram, :string
    add_column :partners, :website, :string
  end
end
